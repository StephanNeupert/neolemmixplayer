This format hasn't actually been implemented in NeoLemmix itself yet. It should be soon.

Doesn't share a lot of NL code, it's more closely based off GSTool's code. It does use
PngInterface and LemNeoParser from NL's own code though. May use more of NL's code once
NL itself supports the new graphic set formats.